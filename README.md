**Main:** [![main](https://gitlab.com/mdroogh/myweb/badges/main/pipeline.svg)](https://gitlab.com/mdroogh/myweb/-/commits/main) 

**Shadow:** [![shadow](https://gitlab.com/mdroogh/myweb/badges/shadow/pipeline.svg)](https://gitlab.com/mdroogh/myweb/-/commits/shadow) 

# myweb implementation group-7-shadow-deployment


This project is based on: [https://gitlab.com/remla-course/2021/myweb](https://gitlab.com/remla-course/2021/myweb),
its purpose is to extend the pipeline with 'Shadow-deployment' of the application.

This repository deploys to: **[35.239.46.43](35.239.46.43)**.

The SMS application can be found at: [35.239.46.43/sms/](35.239.46.43/sms/).

Metrics can be viewed using Prometheus at: [34.72.240.170:9090](34.72.240.170:9090).

# Pipeline instructions

The ways to deploy a new version of the main or shadow model respectively are:

-   Create a new tag: the 'main' branch will be built, published and finally deployed to Kubernetes and can be found at ip address: [35.239.46.43](35.239.46.43) (it uses the latest model from the [https://gitlab.com/mdroogh/mymodels](https://gitlab.com/mdroogh/mymodels)) repository. 

-   Push to the shadow branch: the shadow branch will be built, published and finally deployed to Kubernetes. All traffic to the IP address mentioned above is shadowed to the shadow service.

# Pipeline Merge Request

A special pipeline is triggered when one opens a Merge Request with the 'shadow' branch as source branch and 'main' branch as target.
The pipeline fails if one of the following conditions is not met:

-   The main application has not been accessed more than 15 times.
-   The shadow application has not been (shadow) accessed more than 15 times.
-   The shadow application has an accuracy which is more than 10 percentage points lower than the main application's accuracy.


# Setup guide
We assume one has a GitLab repository, GitLab runner, and a Google Kubernetes account:

-   Create a cluster in GitLab under the 'infrastructure' tab (it is straightforward, but for help see: [https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html)), specify an environment!
-   Connect to the cluster in the Google Kubernetes Console
-   Execute the suggested command which starts with 'gcloud container clusters get-credentials ...'
-   Execute the following commands in the Google kubernetes console:
    -   `helm repo add datawire https://www.getambassador.io`
    -   `helm install ambassador datawire/ambassador`
    -   `helm repo add prometheus-community https://prometheus-community.github.io/helm-charts`
    -   `helm repo update`
    -   `helm install prometheus prometheus-community/kube-prometheus-stack`
- Change the Prometheus service type from 'clusterIP' to 'LoadBalancer' to make it accessible from outside (`kubectl edit svc prometheus-kube-prometheus-prometheus`)
- Apply the myweb file (kubectl apply -f myweb.yml)
- Apply the myweb-shadow file (kubectl apply -f myweb-shadow.yml)
- Run the gitlab-ci (Make sure to use the same environment in the deploy stage as defined above)

# Configuring this project for use with another application:
-   In the *myweb.yml* and *myweb-shadow.yml* files change:
    -   `mymodels` to the used model name
    -   `registry.gitlab.com/mdroogh/mymodels:latest` to the image registry of the model
    -   `myweb` to the used project name
    -   `registry.gitlab.com/mdroogh/myweb:latest` to the image registry of the project
    -   (Change the number of replicas if required)
    -   (Change the ports if required)

-   In the gitlab-ci.yml file:
    -   environment
    -   (pull other metrics from ambassador)
    -   (change the checks in the 'merge-request' script to other bash checks)
    -   (change the naming from `shadow` branch to the name of the branch to deploy in shadow mode)
